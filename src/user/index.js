import "dotenv/config";
import { QueryCommand, PutItemCommand } from "@aws-sdk/client-dynamodb";
import { v4 as uuidv4 } from "uuid";
import bcrypt from 'bcrypt';
import validator from 'validator';
import randToken from 'rand-token';
import jwt from 'jsonwebtoken';
import { ddbClient } from "../ddbClient";

const add = async (req, res) => {
  // Destructuring
  const { body } = req || {};
  const { name, lastname, birthdate, country, city, email, password } = body || {};
  // Validate data
  if (!name || !validator.isAlpha(name)) { res.json({ status: 400, message: 'INVALID_NAME' }); return; };
  if (!lastname || !validator.isAlpha(lastname)) { res.json({ status: 400, message: 'INVALID_LASTNAME' }); return; };
  if (!birthdate || !validator.isNumeric(birthdate)) { res.json({ status: 400, message: 'INVALID_BIRTHDATE' }); return; };
  if (!country || !validator.isAlpha(country)) { res.json({ status: 400, message: 'INVALID_COUNTRY' }); return; };
  if (!city || !validator.isAlpha(city)) { res.json({ status: 400, message: 'INVALID_CITY' }); return; };
  if (!email || !validator.isEmail(email)) { res.json({ status: 400, message: 'INVALID_EMAIL' }); return; };
  if (!password || validator.isEmpty(password)) { res.json({ status: 400, message: 'INVALID_PASSWORD' }); return; };

  try {
    const userQuery = await ddbClient.send(new QueryCommand({
      TableName: 'Users',
      IndexName: 'users-email-index',
      ScanIndexForward: true,
      ExpressionAttributeValues: {
        ":email": { S: email }
      },
      KeyConditionExpression: 'email = :email'
    }));
    if (userQuery.Count > 0) {
      res.status(400).json({message: 'EMAIL_ALREADY_EXISTS' });
      return;
    }
    // Generate uuid
    const id = uuidv4();
    // Hardcode init role
    const role = 'PLAYER';
    // Generate password hash
    const passwordHash = await bcrypt.hash(password, 10);
    await ddbClient.send(new PutItemCommand({
      TableName: 'Users',
      Item: {
        id: { S: id },
        name: { S: name },
        lastname: { S: lastname },
        birthdate: { S: birthdate },
        country: { S: country },
        city: { S: city },
        role: { S: role },
        email: { S: email },
        password: { S: passwordHash }
      },
    }));
    const response = {
      user: {
        id: id,
        name: name
      }
    };
    res.status(200).json(response);
  } catch (error) {
    console.error(error);
    res.status(400).json({ message: error.message });
  }
};

const login = async (req, res) => {
  // Destructuring
  const { body } = req || {};
  const { email, password } = body || {};
  // Validate data
  if (!email || !validator.isEmail(email)) { res.json({ status: 400, message: 'INVALID_EMAIL' }); return; };
  if (!password || validator.isEmpty(password)) { res.json({ status: 400, message: 'INVALID_PASSWORD' }); return; };

  try {
    const userQuery = await ddbClient.send(new QueryCommand({
      TableName: 'Users',
      IndexName: 'users-email-index',
      ScanIndexForward: true,
      ExpressionAttributeValues: {
        ":email": { S: email }
      },
      KeyConditionExpression: 'email = :email'
    }));
    if (userQuery.Count === 0) {
      res.status(400).json({ message: 'EMAIL_DOES_NOT_MATCH' });
      return;
    }
    const user = userQuery.Items.length > 0 && userQuery.Items[0];
    const validPassword = await bcrypt.compare(password, user.password.S);
		if(!validPassword){
			res.status(400).json({message: "INVALID_PASSWORD"});
			return;
    }
    // Create access token
    const accessToken = jwt.sign({
      aud: "AUTH",
      _id: user.id.S,
      role: "PLAYER",
      email: user.email.S
    }, process.env.JWT_PRIVATE_KEY, {
      expiresIn: "1h",
      // algorithm: "RS256"
    });

    // Create new refresh token
    const refreshToken = randToken.uid(256);
    const response = {
      user: {
        id: user.id.S,
        name: user.name.S
      },
      accessToken: accessToken,
      refreshToken: refreshToken
    };
    res.status(200).json(response);
  } catch (error) {
    console.error(error);
    res.status(400).json({ message: error.message });
  }
};

export { add, login };