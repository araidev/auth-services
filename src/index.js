
import "dotenv/config";
import express from 'express';
import helmet from 'helmet';
import router from './routes';

const app = express();

// https://expressjs.com/en/advanced/best-practice-security.html
app.use(helmet());

app.use(express.json());
app.use(router);


app.listen(process.env.NODE_PORT, () => {
  console.info(`Server is running on PORT ${process.env.NODE_PORT}`)
});