var myHeaders = new Headers();
myHeaders.append("Content-Type", "application/json");

var raw = JSON.stringify({
  "email": "cristian@araidev.com",
  "password": "hola1234"
});

var requestOptions = {
  method: 'POST',
  headers: myHeaders,
  body: raw,
  redirect: 'follow'
};

fetch("http://localhost:3000/user/login", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));

const result = {
    "user": {
        "id": "267d255d-c086-4d46-945b-7807cc7a9d2a",
        "name": "Cristian"
    },
    "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJBVVRIIiwiX2lkIjoiMjY3ZDI1NWQtYzA4Ni00ZDQ2LTk0NWItNzgwN2NjN2E5ZDJhIiwicm9sZSI6IlBMQVlFUiIsImVtYWlsIjoiY3Jpc3RpYW5AYXJhaWRldi5jb20iLCJpYXQiOjE2NDQ4OTE1OTcsImV4cCI6MTY0NDg5NTE5N30.q45Fp7VKSSlhQ7BV_O9EJ681cEnPxLVhsbJYvMfSTF4",
    "refreshToken": "FYil5SqRTku4w11GbdIzM9pLdIMMThnK9u3dFlpNZcxxB8Qun7EgDzKeAYQQv6xfGHDiwzeTgoYn5xFfNKjYceNkJaR0uLPRUYk83NHd05BfUtfMGeCDBK1lZi2gf1jIcWbRDBu5H3P9nMW9R0qBJZzjhQBzkRtg7rHGKxsGPSruI0qD3gsOt3U9Z8Dhc5yL7eb6Y5XwzOeseQ48LSElVIN6inlNIwzlSsQHopYpdFC42KNxe7qDfxQCj3WTGwLG"
}