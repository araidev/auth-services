var myHeaders = new Headers();
myHeaders.append("Content-Type", "application/json");

var raw = JSON.stringify({
  "name": "Cristian",
  "lastname": "Paniagua",
  "birthdate": "1644712294",
  "country": "PARAGUAY",
  "city": "VILLARRICA",
  "email": "cristian@araidev.com",
  "password": "hola1234"
});

var requestOptions = {
  method: 'POST',
  headers: myHeaders,
  body: raw,
  redirect: 'follow'
};

fetch("http://localhost:3000/user/add", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));

const result = {
  "user": {
    "id": "9eb8456b-678e-4ab5-bb48-58bc97066f3f",
    "name": "Cristian"
  }
};