import { Router } from "express";
import * as user from './user';

const router = Router();

// Public
router.post('/user/add', user.add);
router.post('/user/login', user.login);
// Private

export default router;