import { QueryCommand, PutItemCommand } from "@aws-sdk/client-dynamodb";
import { v4 as uuidv4 } from "uuid";
import bcrypt from 'bcrypt';
import validator from 'validator';
import randToken from 'rand-token';
import jwt from 'jsonwebtoken';
import { ddbClient } from "../../ddbClient";

const data = {
  name: "Cristian",
  lastname: "Paniagua",
  birthdate: "1644712294",
  country: "PARAGUAY",
  city: "VILLARRICA",
  email: uuidv4() + "@araidev.com",
  password: "hola1234"
};

// beforeEach(async () => await createUsers());
// afterEach(async () => await dbHandler.clearDatabase());

describe('POST /user/add', () => {
  test('should respond with 200 when provided with valid data', async() => {
    // Destructuring
    const { name, lastname, birthdate, country, city, email, password } = data || {};
    // Generate uuid
    const id = uuidv4();
    // Hardcode init role
    const role = 'PLAYER';
    // Generate password hash
    const passwordHash = await bcrypt.hash(password, 10);

    try {
      await ddbClient.send(new PutItemCommand({
        TableName: 'Users',
        Item: {
          id: { S: id },
          name: { S: name },
          lastname: { S: lastname },
          birthdate: { S: birthdate },
          country: { S: country },
          city: { S: city },
          role: { S: role },
          email: { S: email },
          password: { S: passwordHash }
        },
      }));
      const response = {
        user: {
          id: id,
          name: name,
        }
      };
      expect(id).toBe(response.user.id);
    } catch (error) {
      console.error(error);
    }
  });

  test('should respond with 400 when provided with duplicate email', async () => {
    // Destructuring
    const { email } = data || {};

    try {
      const userQuery = await ddbClient.send(new QueryCommand({
        TableName: 'Users',
        IndexName: 'users-email-index',
        ScanIndexForward: true,
        FilterExpression: "attribute_exists(#email)",
        ExpressionAttributeValues: {
          ":email": {
              S: email
          }
      },
        ExpressionAttributeNames: {
          '#email': email
        },
        KeyConditionExpression: 'email = :email'
      }));
      expect(1).toBe(userQuery.ScannedCount);
    } catch (error) {
      console.error(error);
    }
  });

  test('should respond with 400 when provided with invalid password', async () => {
    // Force new value
    const EMPTY = '';
    data.password = EMPTY;
    // Destructuring
    const { password } = data || {};

    expect(true).toBe(validator.isEmpty(password));
  });
});

describe('POST /user/login', () => {
  test('should respond with 200 when provided with valid data', async () => {
    // Force new value
    data.email = 'cristian@araidev.com';
    data.password = 'hola1234';
    // Destructuring
    const { email, password } = data || {};

    try {
      const userQuery = await ddbClient.send(new QueryCommand({
        TableName: 'Users',
        IndexName: 'users-email-index',
        ScanIndexForward: true,
        ExpressionAttributeValues: {
          ":email": { S: email }
        },
        KeyConditionExpression: 'email = :email'
      }));
      const user = userQuery.Items.length > 0 && userQuery.Items[0];
      const validPassword = await bcrypt.compare(password, user.password.S);
      if(!validPassword){
        return;
      }
      // Create access token
      const accessToken = jwt.sign({
        aud: "AUTH",
        _id: user.id.S,
        role: "PLAYER",
        email: user.email.S
      }, process.env.JWT_PRIVATE_KEY, {
        expiresIn: "1h",
        // algorithm: "RS256"
      });
      // Create new refresh token
      const refreshToken = randToken.uid(256);
      const response = {
        user: {
          id: user.id.S,
          name: user.name.S
        },
        accessToken: accessToken,
        refreshToken: refreshToken
      };
      expect(200).toBe(200);
    } catch (error) {
      console.error(error);
    }
  });

  test('should respond with 400 when provided with invalid email', async () => {
    // Force new value
    const emailInv = 'cristian_NO_EXISTS@araidev.com';
    data.email = emailInv;
    // Destructuring
    const {email } = data || {};
    // Validate data
    if (!email || !validator.isEmail(email)) { res.json({ status: 400, message: 'INVALID_EMAIL' }); return; };
    try {
      const userQuery = await ddbClient.send(new QueryCommand({
        TableName: 'Users',
        IndexName: 'users-email-index',
        ScanIndexForward: true,
        ExpressionAttributeValues: {
          ":email": { S: email }
        },
        KeyConditionExpression: 'email = :email'
      }));
      expect(0).toBe(userQuery.Count);
    } catch (error) {
      console.error(error);
    }
  });

  test('should respond with 400 when provided with invalid password', async () => {
    // Force new value
    const EMPTY = '';
    data.password = EMPTY;
    // Destructuring
    const { password } = data || {};
    // Validate data
    expect(true).toBe(validator.isEmpty(password));
  });
});